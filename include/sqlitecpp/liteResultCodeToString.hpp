//          Copyright Mark Grimes 2017 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

/** @file
 * @brief Declaration for the sqlitecpp::liteResultCodeToString free function.
 *
 * @author Mark Grimes (mark.grimes@datanomita.com).
 * @date 28/May/2021.
 */
#pragma once
#include <system_error>

namespace sqlitecpp
{
    /** @brief Converts an sqlite return code to a string, for use in error reporting
     *
     * So e.g. given SQLITE_ABORT the function returns the string "SQLITE_ABORT".
     *
     * @author Mark Grimes (mark.grimes@datanomita.com).
     * @date 28/May/2021.
     */
    const char* liteResultCodeToString( int result );

    /** @brief An error category for using the SQLite int error return codes as std::error_condition. */
    const std::error_category& sqlite_error_category() noexcept;

} // end of namespace sqlitecpp
