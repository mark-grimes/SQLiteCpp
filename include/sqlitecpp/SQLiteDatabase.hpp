//          Copyright Mark Grimes 2017 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

/** @file
 * @brief Header declaration for the sqlitecpp::SQLiteDatabase class.
 *
 * @author Mark Grimes (mark.grimes@datanomita.com).
 * @date 28/May/2021.
 */
#pragma once

#include <string>
#include <system_error>
#include <functional> // for std::function
//
// Forward declarations
//
struct sqlite3;
namespace sqlitecpp
{
    class SQLiteStatement;
}


namespace sqlitecpp
{
    /** @brief Class to help with access to sqlite database C interface
     *
     * RAII compliant so it's exception safe.
     *
     * @author Mark Grimes (mark.grimes@datanomita.com).
     * @date 28/May/2021.
     */
    class SQLiteDatabase
    {
    public:
        SQLiteDatabase( const char* pFilename );
        SQLiteDatabase( const char* pFilename, std::error_code& error );
        SQLiteDatabase( const std::string& filename ) : SQLiteDatabase(filename.c_str()) {}
        SQLiteDatabase( const std::string& filename, std::error_code& error ) : SQLiteDatabase( filename.c_str(), error ) {}

        /** @brief Open a database at the provided filename, with the flags
         *
         * The flags are those used in the sqlite3_open_v2() call. See
         * https://www.sqlite.org/c3ref/open.html for further details. */
        SQLiteDatabase( const char* pFilename, int flags );
        SQLiteDatabase( const char* pFilename, int flags, std::error_code& error );
        SQLiteDatabase( const std::string& filename, int flags ) : SQLiteDatabase( filename.c_str(), flags ) {}
        SQLiteDatabase( const std::string& filename, int flags, std::error_code& error ) : SQLiteDatabase( filename.c_str(), flags, error ) {}

        enum class OpenFlags{
            /** @brief Equivalent to SQLITE_OPEN_READONLY.
             *
             * The database is opened in read-only mode. If the database does not already exist,
             * an error is returned. */
            ReadOnly,

            /** @brief Equivalent to SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE.
             *
             * The database is opened for reading and writing, and is created if it does not
             * already exist. */
            ReadWrite,

            /** @brief Equivalent to SQLITE_OPEN_READWRITE.
             *
             * The database is opened for reading and writing if possible, or reading only if
             * the file is write protected by the operating system. In either case the database
             * must already exist, otherwise an error is returned. */
            ExistingReadWrite
        };
        /** @brief Open a database using the read/write behaviour given by the OpenFlags enum
         *
         * This constructor is purely so that the behaviour for flags like SQLITE_OPEN_READONLY
         * and SQLITE_OPEN_READWRITE can be obtained without having to include the sqlite3.h
         * header. */
        SQLiteDatabase( const char* pFilename, SQLiteDatabase::OpenFlags flags );
        SQLiteDatabase( const char* pFilename, SQLiteDatabase::OpenFlags flags, std::error_code& error );
        SQLiteDatabase( const std::string& filename, SQLiteDatabase::OpenFlags flags ) : SQLiteDatabase( filename.c_str(), flags ) {}
        SQLiteDatabase( const std::string& filename, SQLiteDatabase::OpenFlags flags, std::error_code& error ) : SQLiteDatabase( filename.c_str(), flags, error ) {}

        // Make sure the object isn't copied incorrectly
        SQLiteDatabase( const SQLiteDatabase& other ) = delete;
        SQLiteDatabase& operator=( const SQLiteDatabase& other ) = delete;
        // Make sure the object can be moved correctly, invalidating the old object
        SQLiteDatabase( SQLiteDatabase&& other );
        SQLiteDatabase& operator=( SQLiteDatabase&& other );

        ~SQLiteDatabase();

        void execute( const char* pCommand );
        void execute( const std::string& command ) { return execute( command.c_str() ); };
        void execute( const char* pCommand, std::function<bool(int,int,char*,char*)> resultsCallback );
        sqlitecpp::SQLiteStatement prepareStatement( const char* pStatement );
        sqlitecpp::SQLiteStatement prepareStatement( const std::string statement );
    private:
        void execute_( const char* pCommand, void* userCallback );
        static int callback_( void *pMyFunction, int argc, char **argv, char **azColName );
        sqlite3* pDatabase_;
    }; // end of class SQLiteDatabase

} // end of namespace sqlitecpp
