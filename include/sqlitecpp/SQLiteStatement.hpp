//          Copyright Mark Grimes 2017 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

/** @file
 * @brief Header declaration for the sqlitecpp::SQLiteStatement class.
 *
 * @author Mark Grimes (mark.grimes@datanomita.com).
 * @date 28/May/2021.
 */
#pragma once

#include <cstddef> // for size_t
#include <utility> // for std::pair
//
// Forward declarations
//
struct sqlite3;
struct sqlite3_stmt;

namespace sqlitecpp
{
    /** @brief Class to handle prepared statements on sqlite databases
     *
     * @author Mark Grimes (mark.grimes@datanomita.com).
     * @date 28/May/2021.
     */
    class SQLiteStatement
    {
    public:
        /** @brief Create an empty object - no method will work on the object until it is assigned to.
         */
        SQLiteStatement();

        // Make sure the object cannot be copied incorrectly
        SQLiteStatement( const SQLiteStatement& other ) = delete;
        SQLiteStatement& operator=( const SQLiteStatement& other ) = delete;
        // Allow objects to be moved
        SQLiteStatement( SQLiteStatement&& other );
        SQLiteStatement& operator=( SQLiteStatement&& other );

        ~SQLiteStatement();

        void bind( const double value, int position );
        void bind( const float value, int position );
        void bind( const int value, int position );
        void bind( const short value, int position );
        void bind( const char* value, int position );
        /** @brief Bind function for MYSQL_TYPE_TINY, i.e. a single byte, not a pointer to an array of bytes */
        void bind( const signed char value, int position );
        void bind( const void* value, int position, int size );
        void bind( std::nullptr_t null, int position );

        void reset();

        enum class StepResult{ Finished, Row };
        sqlitecpp::SQLiteStatement::StepResult step();

        /** @brief Whether or not the given field at the column is NULL or not
         *
         * It is important that this is called **before** any of the getColumnsAs... methods,
         * because they can change the value of the column. */
        bool columnNotNull( int position );

        std::pair<const char*,size_t> getColumnAsText( int position );
        std::pair<const void*,size_t> getColumnAsBlob( int position );
        int getColumnAsInt( int position );
        double getColumnAsDouble( int position );

    private:
        friend class SQLiteDatabase;
        SQLiteStatement( sqlite3_stmt* pStatement, sqlite3* pDatabase );
        sqlite3_stmt* pStatement_;
        sqlite3* pDatabase_; // required to get error messages
    }; // end of class SQLiteStatement

} // end of namespace sqlitecpp
