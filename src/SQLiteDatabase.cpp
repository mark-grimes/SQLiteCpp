//          Copyright Mark Grimes 2017 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

/** @file
 * @brief Definitions for the sqlitecpp::SQLiteDatabase class methods.
 *
 * @author Mark Grimes (mark.grimes@datanomita.com).
 * @date 28/May/2021.
 */
#include "sqlitecpp/SQLiteDatabase.hpp"
#include <stdexcept>
#include <sqlite3.h>
#include "sqlitecpp/SQLiteStatement.hpp"
#include "sqlitecpp/liteResultCodeToString.hpp"

// Use the unnamed namespace for things only used in this file
namespace
{
    /** @brief Converts SQLiteDatabase::OpenFlags to sqlite C interface int flags
     */
    int sqliteFlagsFromEnum( sqlitecpp::SQLiteDatabase::OpenFlags flags, std::error_code& error )
    {
        using OpenFlags = sqlitecpp::SQLiteDatabase::OpenFlags;
        switch( flags )
        {
            case OpenFlags::ReadOnly: return SQLITE_OPEN_READONLY;
            case OpenFlags::ReadWrite: return SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
            case OpenFlags::ExistingReadWrite: return SQLITE_OPEN_READWRITE;
            default:
                error = std::make_error_code( std::errc::invalid_argument );
                return 0;
        }
    } // end of function sqliteFlagsFromEnum

    // Throwing version
    int sqliteFlagsFromEnum( sqlitecpp::SQLiteDatabase::OpenFlags flags )
    {
        std::error_code error;
        int result = sqliteFlagsFromEnum( flags, error );
        if( error ) throw std::runtime_error( "Cannot convert unknown SQLiteDatabase::OpenFlags value to sqlite int flags" );
        return result;
    } // end of function sqliteFlagsFromEnum
} // end of the unnamed namespace

sqlitecpp::SQLiteDatabase::SQLiteDatabase( const char* pFilename )
    : pDatabase_(nullptr)
{
    int result = sqlite3_open( pFilename, &pDatabase_ );
    if( result != SQLITE_OK )
    {
        std::string errorMessage = sqlite3_errmsg(pDatabase_);
        sqlite3_close( pDatabase_ );
        throw std::runtime_error( "Can't open database: " + errorMessage );
    }
}

sqlitecpp::SQLiteDatabase::SQLiteDatabase( const char* pFilename, std::error_code& error )
    : pDatabase_(nullptr)
{
    int result = sqlite3_open( pFilename, &pDatabase_ );
    if( result != SQLITE_OK )
    {
        error = std::error_code( result, sqlitecpp::sqlite_error_category() );
        sqlite3_close( pDatabase_ );
    }
}

sqlitecpp::SQLiteDatabase::SQLiteDatabase( const char* pFilename, int flags )
    : pDatabase_(nullptr)
{
    int result = sqlite3_open_v2( pFilename, &pDatabase_, flags, nullptr );
    if( result != SQLITE_OK )
    {
        std::string errorMessage = sqlite3_errmsg(pDatabase_);
        sqlite3_close( pDatabase_ );
        throw std::runtime_error( "Can't open database: " + errorMessage );
    }
}

sqlitecpp::SQLiteDatabase::SQLiteDatabase( const char* pFilename, int flags, std::error_code& error )
    : pDatabase_(nullptr)
{
    if( error ) return; // Check in case sqliteFlagsFromEnum went wrong

    int result = sqlite3_open_v2( pFilename, &pDatabase_, flags, nullptr );
    if( result != SQLITE_OK )
    {
        error = std::error_code( result, sqlitecpp::sqlite_error_category() );
        sqlite3_close( pDatabase_ );
    }
}

sqlitecpp::SQLiteDatabase::SQLiteDatabase( const char* pFilename, SQLiteDatabase::OpenFlags flags )
    : SQLiteDatabase( pFilename, ::sqliteFlagsFromEnum(flags) )
{
    // Everything delegated to the other constructor
}

sqlitecpp::SQLiteDatabase::SQLiteDatabase( const char* pFilename, SQLiteDatabase::OpenFlags flags, std::error_code& error )
    : SQLiteDatabase( pFilename, ::sqliteFlagsFromEnum( flags, error ), error )
{
    // Everything delegated to the other constructor
}

sqlitecpp::SQLiteDatabase::SQLiteDatabase( SQLiteDatabase&& other )
    : pDatabase_( other.pDatabase_ )
{
    other.pDatabase_ = nullptr;
}

sqlitecpp::SQLiteDatabase& sqlitecpp::SQLiteDatabase::operator=( SQLiteDatabase&& other )
{
    if( this != &other )
    {
        if( pDatabase_ ) sqlite3_close( pDatabase_ );
        pDatabase_ = other.pDatabase_;
        other.pDatabase_ = nullptr;
    }
    return *this;
}

sqlitecpp::SQLiteDatabase::~SQLiteDatabase()
{
    if( pDatabase_ ) sqlite3_close( pDatabase_ );
}

void sqlitecpp::SQLiteDatabase::execute( const char* pCommand )
{
    execute_( pCommand, nullptr );
}

void sqlitecpp::SQLiteDatabase::execute( const char* pCommand, std::function<bool(int,int,char*,char*)> resultsCallback )
{
    execute_( pCommand, &resultsCallback );
}

void sqlitecpp::SQLiteDatabase::execute_( const char* pCommand, void* userCallback )
{
    //std::cout << "Executing command " << pCommand << std::endl;
    char* pErrorMsg = 0;
    int result = sqlite3_exec( pDatabase_, pCommand, (userCallback ? callback_ : nullptr), userCallback, &pErrorMsg );
    if( result != SQLITE_OK )
    {
        std::string errorMessage(pErrorMsg);
        sqlite3_free( pErrorMsg );
        throw std::runtime_error(std::string("Error executing command '")+pCommand+"': "+errorMessage);
    }
}

sqlitecpp::SQLiteStatement sqlitecpp::SQLiteDatabase::prepareStatement( const char* pCommand )
{
    sqlite3_stmt* pStatement = nullptr;
    int result;
    if( (result = sqlite3_prepare_v2( pDatabase_, pCommand, static_cast<int>( std::char_traits<char>::length(pCommand) ), &pStatement, NULL )) != SQLITE_OK )
    {
        throw std::runtime_error( std::string("Can't prepare SQLiteStatement '")+pCommand+"': "+sqlitecpp::liteResultCodeToString(result) );
    }
    return SQLiteStatement( pStatement, pDatabase_ );
}

sqlitecpp::SQLiteStatement sqlitecpp::SQLiteDatabase::prepareStatement( const std::string statement )
{
    return prepareStatement( statement.c_str() );
}

int sqlitecpp::SQLiteDatabase::callback_( void* pMyFunction, int argc, char** argv, char** azColName )
{
    if( pMyFunction == nullptr ) return 0;
    std::function<bool(int,int,char*,char*)>& callback = *reinterpret_cast<std::function<bool(int,int,char*,char*)>*>(pMyFunction);

    bool keepGoing = true;
    for( int index = 0; index < argc && keepGoing; ++index )
    {
        keepGoing = callback( index, argc, argv[index], azColName[index] );
    }

    return (keepGoing ? 0 : 1);
}
