//          Copyright Mark Grimes 2017 - 2021.
// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE_1_0.txt or copy at
//          https://www.boost.org/LICENSE_1_0.txt)

/** @file
 * @brief Definitions for the sqlitecpp::SQLiteStatement class methods.
 *
 * @author Mark Grimes (mark.grimes@datanomita.com).
 * @date 28/May/2021.
 */
#include "sqlitecpp/SQLiteStatement.hpp"
#include <stdexcept>
#include <string>
#include <sqlite3.h>
#include "sqlitecpp/liteResultCodeToString.hpp"

sqlitecpp::SQLiteStatement::SQLiteStatement()
    : pStatement_(nullptr), pDatabase_(nullptr)
{
    // No operation besides the initialiser list
}

sqlitecpp::SQLiteStatement::SQLiteStatement( sqlite3_stmt* pStatement, sqlite3* pDatabase )
    : pStatement_(pStatement), pDatabase_(pDatabase)
{
    // No operation besides the initialiser list
}

sqlitecpp::SQLiteStatement::SQLiteStatement( SQLiteStatement&& other )
    : pStatement_( other.pStatement_ ), pDatabase_( other.pDatabase_ )
{
    other.pStatement_ = nullptr;
    other.pDatabase_ = nullptr;
}

sqlitecpp::SQLiteStatement& sqlitecpp::SQLiteStatement::operator=( SQLiteStatement&& other )
{
    if( this != &other )
    {
        if( pStatement_ ) sqlite3_finalize(pStatement_);
        pStatement_ = other.pStatement_;
        pDatabase_ = other.pDatabase_;
        other.pStatement_ = nullptr;
        other.pDatabase_ = nullptr;
    }
    return *this;
}

sqlitecpp::SQLiteStatement::~SQLiteStatement()
{
    if( pStatement_ ) sqlite3_finalize(pStatement_);
}

void sqlitecpp::SQLiteStatement::bind( const double value, int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::bind called on a null object"); // if constructed empty

    int result;
    if( (result = sqlite3_bind_double( pStatement_, position, value )) != SQLITE_OK )
    {
        throw std::runtime_error( std::string("Can't bind double to SQLiteStatement: ")+liteResultCodeToString(result) );
    }
}

void sqlitecpp::SQLiteStatement::bind( const float value, int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::bind called on a null object"); // if constructed empty

    int result;
    if( (result = sqlite3_bind_double( pStatement_, position, value )) != SQLITE_OK )
    {
        throw std::runtime_error( std::string("Can't bind float to SQLiteStatement: ")+liteResultCodeToString(result) );
    }
}

void sqlitecpp::SQLiteStatement::bind( const int value, int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::bind called on a null object"); // if constructed empty

    int result;
    if( (result = sqlite3_bind_int( pStatement_, position, value )) != SQLITE_OK )
    {
        throw std::runtime_error( std::string("Can't bind int to SQLiteStatement: ")+liteResultCodeToString(result) );
    }
}

void sqlitecpp::SQLiteStatement::bind( const short value, int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::bind called on a null object"); // if constructed empty

    int result;
    if( (result = sqlite3_bind_int( pStatement_, position, value )) != SQLITE_OK )
    {
        throw std::runtime_error( std::string("Can't bind short to SQLiteStatement: ")+liteResultCodeToString(result) );
    }
}

void sqlitecpp::SQLiteStatement::bind( const char* value, int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::bind called on a null object"); // if constructed empty

    int result;
    if( (result = sqlite3_bind_text( pStatement_, position, value, -1, nullptr )) != SQLITE_OK )
    {
        throw std::runtime_error( std::string("Can't bind text to SQLiteStatement: ")+liteResultCodeToString(result) );
    }
}

/// Note that this function is for MYSQL_TYPE_TINY, i.e. a single byte, not a pointer to an array of bytes
void sqlitecpp::SQLiteStatement::bind( const signed char value, int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::bind called on a null object"); // if constructed empty

    int result;
    if( (result = sqlite3_bind_int( pStatement_, position, value )) != SQLITE_OK )
    {
        throw std::runtime_error( std::string("Can't bind signed char to SQLiteStatement: ")+liteResultCodeToString(result) );
    }
}

void sqlitecpp::SQLiteStatement::bind( const void* value, int position, int size )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::bind called on a null object"); // if constructed empty

    int result;
    if( (result = sqlite3_bind_blob( pStatement_, position, value, size, nullptr )) != SQLITE_OK )
    {
        throw std::runtime_error( std::string("Can't bind blob to SQLiteStatement: ")+liteResultCodeToString(result) );
    }
}

void sqlitecpp::SQLiteStatement::bind( std::nullptr_t null, int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::bind called on a null object"); // if constructed empty

    int result;
    if( (result = sqlite3_bind_null( pStatement_, position )) != SQLITE_OK )
    {
        throw std::runtime_error( std::string("Can't bind null to SQLiteStatement: ")+liteResultCodeToString(result) );
    }
}

void sqlitecpp::SQLiteStatement::reset()
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::reset called on a null object"); // if constructed empty

    sqlite3_reset(pStatement_);
}

sqlitecpp::SQLiteStatement::StepResult sqlitecpp::SQLiteStatement::step()
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::step called on a null object"); // if constructed empty

    int result = sqlite3_step(pStatement_);
    if( result == SQLITE_ROW ) return StepResult::Row;
    else if( result == SQLITE_DONE ) return StepResult::Finished;
    else if( result == SQLITE_ERROR )
    {
        throw std::runtime_error( std::string("Can't step SQLiteStatement: ")+sqlite3_errmsg(pDatabase_) );
    }
    else
    {
        throw std::runtime_error( std::string("Can't step SQLiteStatement: ")+liteResultCodeToString(result) );
    }
}

bool sqlitecpp::SQLiteStatement::columnNotNull( int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::columnNotNull called on a null object"); // if constructed empty

    return SQLITE_NULL != sqlite3_column_type( pStatement_, position );
}

std::pair<const char*,size_t> sqlitecpp::SQLiteStatement::getColumnAsText( int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::getColumnAsText called on a null object"); // if constructed empty

    std::pair<const char*,size_t> result;
    result.first = reinterpret_cast<const char*>( sqlite3_column_text( pStatement_, position ) );
    result.second = sqlite3_column_bytes( pStatement_, position );

    return result;
}

std::pair<const void*,size_t> sqlitecpp::SQLiteStatement::getColumnAsBlob( int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::getColumnAsBlob called on a null object"); // if constructed empty

    std::pair<const void*,size_t> result;
    result.first = sqlite3_column_blob( pStatement_, position );
    result.second = sqlite3_column_bytes( pStatement_, position );

    return result;
}

int sqlitecpp::SQLiteStatement::getColumnAsInt( int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::getColumnAsInt called on a null object"); // if constructed empty

    return sqlite3_column_int( pStatement_, position );
}

double sqlitecpp::SQLiteStatement::getColumnAsDouble( int position )
{
    if( !pStatement_ ) throw std::runtime_error("SQLiteStatement::getColumnAsDouble called on a null object"); // if constructed empty

    return sqlite3_column_double( pStatement_, position );
}
