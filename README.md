# Simple C++ wrapper around the SQLite C library

## Licence

Licensed under the Boost Software Licence, see [LICENSE_1_0.txt](/LICENSE_1_0.txt). This is a very permissive licence that doesn't require attribution once the code is compiled.

## About

This is a very simple wrapper around the SQLite C interface that provides some RAII semantics to ease the use of the library. It allows preparing statements etc.
